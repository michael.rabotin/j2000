# J2000

**Page wiki du groupe d'animation de J2000.**

Pour visualiser la première page du wiki, allez dans le panneau de gauche, dans Plan, cliquer sur wiki et vous aurez accès à la page d'accueil du wiki.


Vous pouvez également épingler la page d'accueil du wiki pour le faire apparaître en raccourci dans la partie "**Pinned**" du panneau de gauche. Pour cela, dans Plan, sur la ligne wiki laissez la souris quelques secondes pour faire apparaître le symbole **Epingle** et cliquer dessus.

Si vous avez besoin d'autoriser l'accès de ce wiki à une personne extérieure à la fédération RENATER, faire une demande à support-forgemia@inrae.fr
